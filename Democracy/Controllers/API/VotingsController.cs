﻿using Democracy.Classes;
using Democracy.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Democracy.Controllers.API
{
    [RoutePrefix("api/Votings")]
    public class VotingsController : ApiController
    {
        private DemocracyContext db = new DemocracyContext();

        [HttpGet]
        [Route("{userId}")]
        public IHttpActionResult MyVotings(int userId)
        {
            var user = db.Users.Find(userId);
            if (user == null)
            {
                return this.BadRequest("User not found");
            }

            var votings = Utilities.MyVotings(user);
            var votingsResponse = new List<VotingResponse>();
            foreach (var voting in votings)
            {
                User winner = null;
                if (voting.CandidateWinId != 0)
                {
                    winner = db.Users.Find(voting.CandidateWinId);
                }

                var candidatesResponse = new List<CandidateResponse>();
                foreach (var candidate in voting.Candidates)
                {
                    candidatesResponse.Add(new CandidateResponse
                    {
                        CandidateId = candidate.CandidateId,
                        QuantityVotes = candidate.QuantityVotes,
                        User = candidate.User,
                    });
                }

                votingsResponse.Add(new VotingResponse
                {
                    DateTimeEnd = voting.DateTimeEnd,
                    DateTimeStart = voting.DateTimeStart,
                    Description = voting.Description,
                    IsEnabledBlankVote = voting.IsEnabledBlankVote,
                    IsForAllUsers = voting.IsForAllUsers,
                    Candidates = candidatesResponse,
                    State = voting.State,
                    VotingId = voting.VotingId,

                });
            }

            return this.Ok(votingsResponse);
        }

        [HttpGet]
        [Route("results")]
        public IHttpActionResult Results()
        {
            var state = Utilities.GetState("Closed");
            var votings = db.Votings
                        .Where(v => v.StateId == state.StateId)
                        .Include(v => v.State)
                        .Include(v => v.Candidates.Select(c => c.User))
                        .ToList();
            var votingsResponse = new List<VotingResponse>();
            var db2 = new DemocracyContext();
            foreach (var voting in votings)
            {
                User winner = null;
                if (voting.CandidateWinId != 0)
                {
                    winner = db2.Users.Find(voting.CandidateWinId);
                }

                var candidatesResponse = new List<CandidateResponse>();
                foreach (var candidate in voting.Candidates)
                {
                    candidatesResponse.Add(new CandidateResponse
                    {
                        CandidateId = candidate.CandidateId,
                        QuantityVotes = candidate.QuantityVotes,
                        User = candidate.User,
                    });
                }

                votingsResponse.Add(new VotingResponse
                {
                    DateTimeEnd = voting.DateTimeEnd,
                    DateTimeStart = voting.DateTimeStart,
                    Description = voting.Description,
                    IsEnabledBlankVote = voting.IsEnabledBlankVote,
                    IsForAllUsers = voting.IsForAllUsers,
                    Candidates = candidatesResponse,
                    State = voting.State,
                    VotingId = voting.VotingId,
                    Winner = winner,
                    QuantityBlankVotes = voting.QuantityBlankVotes,
                    QuantityVotes = voting.QuantityVotes,
                    Remarks = voting.Remarks,
                });
            }

            return this.Ok(votingsResponse);
        }

        [HttpPost]
        [Route("{userId}/{candidateId}/{votingId}")]
        public IHttpActionResult VoteForCandidate(int userId, int candidateId, int votingId)
        {
            var user = db.Users.Find(userId);

            var candidate = db.Candidates.Find(candidateId);

            var voting = db.Votings.Find(votingId);

            using (var transaction = db.Database.BeginTransaction())
            {
                var votingCountActual = db.VotingDetails.Where(vd => vd.VotingId == voting.VotingId).Count();
                var candidateCountActual = db.VotingDetails.Where(vd => vd.VotingId == voting.VotingId && vd.CandidateId == candidate.CandidateId).Count();

                var votingDetail = new VotingDetail
                {
                    CandidateId = candidateId,
                    DateTime = DateTime.Now,
                    UserId = userId,
                    VotingId = votingId,
                };

                db.VotingDetails.Add(votingDetail);

                candidate.QuantityVotes = ++candidateCountActual;
                db.Entry(candidate).State = EntityState.Modified;

                voting.QuantityVotes = ++votingCountActual;
                db.Entry(voting).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return BadRequest("Error: " + ex.Message);
                }
            }

            return this.Ok(voting);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}