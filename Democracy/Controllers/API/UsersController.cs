﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Democracy.Classes;
using Democracy.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;

namespace Democracy.Controllers.API
{
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        private DemocracyContext db = new DemocracyContext();

        [Route("Login")]
        public IHttpActionResult Login(JObject form)
        {
            var email = string.Empty;
            var password = string.Empty;
            dynamic jsonObject = form;

            try
            {
                email = jsonObject.email.Value;
                password = jsonObject.password.Value;
            }
            catch
            {
                return this.BadRequest("Incorrect call");
            }

            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.Find(email, password);

            if(userASP == null)
            {
                return this.BadRequest("Incorrect user or password");
            }

            var user = db.Users
                .Where(u => u.UserName == email)
                .FirstOrDefault();

            if (user == null)
            {
                return this.BadRequest("Problem, you better call support");
            }

            return this.Ok(user);
        }

        [HttpPut]
        [Route("ChangePassword/{userId}")]
        public IHttpActionResult ChangePassword(int userId, JObject form)
        {
            var oldPassword = string.Empty;
            var newPassword = string.Empty;

            dynamic jsonObject = form;

            try
            {
                oldPassword = jsonObject.oldPassword.Value;
                newPassword = jsonObject.newPassword.Value;
            }
            catch
            {
                return this.BadRequest("Incorrect call");
            }

            var user = db.Users.Find(userId);
            if(user == null)
            {
                return BadRequest("User not found");
            }

            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.Find(user.UserName, oldPassword);

            if (userASP == null)
            {
                return this.BadRequest("Incorrect old password");
            }

            var response = userManager.ChangePassword(userASP.Id, oldPassword, newPassword);

            if (response.Succeeded)
            {
                return this.Ok<object>(new
                {
                    Message = "The password was changed successfully"
                });
            }
            else
            {
                return this.BadRequest(response.Errors.ToString());
            }
        }

        [HttpPut]
        public IHttpActionResult PutUser(int id, UserChange user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.UserId)
            {
                return BadRequest();
            }

            var currentUser = db.Users.Find(id);
            if(currentUser == null)
            {
                return BadRequest("User not found");
            }

            var userContext = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));

            var userASP = userManager.Find(currentUser.UserName, user.CurrentPassword);

            if(userASP == null)
            {
                return BadRequest("Password wrong");
            }

            if(currentUser.UserName != user.UserName) //Cambiaron el correo
            {
                Utilities.ChangeUserName(currentUser.UserName, user);
            }

            //var userModel = new User
            //{ 
            //    Address = user.Address,
            //    FirstName = user.FirstName,
            //    Grade = user.Grade,
            //    Group = user.Group,
            //    LastName = user.LastName,
            //    Phone = user.Phone,
            //    Photo = user.Photo,
            //    UserId = user.UserId,
            //    UserName = user.UserName,
            //};

            //db.Entry(userModel).State = EntityState.Modified;

            currentUser.Address = user.Address;
            currentUser.FirstName = user.FirstName;
            currentUser.Grade = user.Grade;
            currentUser.Group = user.Group;
            currentUser.LastName = user.LastName;
            currentUser.Phone = user.Phone;
            currentUser.Photo = user.Photo;
            currentUser.UserName = user.UserName;

            db.Entry(currentUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex.Message.ToString());
            }

            //return StatusCode(HttpStatusCode.NoContent);
            return this.Ok(user);
        }

        [HttpPost]
        public IHttpActionResult PostUser(RegisterUserView userView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new User
            {
                Address = userView.Address,
                FirstName = userView.FirstName,
                LastName = userView.LastName,
                Grade = userView.Grade,
                Group = userView.Group,
                Phone = userView.Phone,
                UserName = userView.UserName,               
            };

            db.Users.Add(user);
            db.SaveChanges();
            Utilities.CreateASPUser(userView);

            return CreatedAtRoute("DefaultApi", new { id = user.UserId }, user);
        }

        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Users.Count(e => e.UserId == id) > 0;
        }
    }
}