﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Democracy.Models
{
    [NotMapped]
    public class UserChange : User
    {
        [Required(ErrorMessage = "The field {0} is required")]
        [StringLength(20, ErrorMessage = "The field {0} can contain minimun {2} and maximun {1} characters", MinimumLength = 8)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }
    }
}